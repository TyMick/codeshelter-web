terraform {
  backend "s3" {
    bucket = "terraformstate.stochastic.io"
    key    = "codeshelter/terraform.tfstate"
    region = "eu-central-1"
  }
}

variable "cloudflare_email" {}
variable "cloudflare_token" {}

variable ipv6_ip { default = "2a01:4f8:1c0c:6109::1" }
variable ipv4_ip { default = "195.201.40.251" }
variable domain { default = "codeshelter.co" }

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "cloudflare_record" "v6_root" {
  domain="${var.domain}"
  type="AAAA"
  name="@"
  proxied="true"
  value="${var.ipv6_ip}"
}

resource "cloudflare_record" "v6_www" {
  domain="${var.domain}"
  type="AAAA"
  name="www"
  proxied="true"
  value="${var.ipv6_ip}"
}

resource "cloudflare_record" "root" {
  domain="${var.domain}"
  type="A"
  name="@"
  proxied="true"
  value="${var.ipv4_ip}"
}

resource "cloudflare_record" "www" {
  domain="${var.domain}"
  type="A"
  name="www"
  proxied="true"
  value="${var.ipv4_ip}"
}

resource "cloudflare_record" "email" {
  name="email"
  domain="${var.domain}"
  type="CNAME"
  value="mailgun.org"
}

resource "cloudflare_record" "api_domainkey" {
  domain="${var.domain}"
  type="TXT"
  name="api._domainkey"
  value="k=rsa;t=s;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbmGbQMzYeMvxwtNQoXN0waGYaciuKx8mtMh5czguT4EZlJXuCt6V+l56mmt3t68FEX5JJ0q4ijG71BGoFRkl87uJi7LrQt1ZZmZCvrEII0YO4mp8sDLXC8g1aUAoi8TJgxq2MJqCaMyj5kAm3Fdy2tzftPCV/lbdiJqmBnWKjtwIDAQAB"
}

resource "cloudflare_record" "mx10" {
  domain="${var.domain}"
  type="MX"
  name="@"
  priority="10"
  value="mxa.mailgun.org"
}

resource "cloudflare_record" "mx20" {
  domain="${var.domain}"
  type="MX"
  name="@"
  priority="20"
  value="mxb.mailgun.org"
}

resource "cloudflare_record" "domainkey" {
  domain="${var.domain}"
  type="TXT"
  name="smtp._domainkey"
  value="k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDo3huUPKMEBzvoskVOu36KQ1rpm99rFavgD/3wHxKIgmyX1GXKFPg6ttS1T3BdGK3a8lFxFm1al4Di27xD3FHOjfb/q2WkFsOj/aFaGbw7jxSOZ9EvUnHTKrSRIJB25+P6bT8bZ6H4pRoS3K4yjMAfnp4i4OHleSv4xa5CECrdEQIDAQAB"
}

resource "cloudflare_record" "spf" {
  domain="${var.domain}"
  type="TXT"
  name="@"
  value="v=spf1 a mx include:mailgun.org include:_spf.elasticemail.com ~all"
}
