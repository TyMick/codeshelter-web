import datetime

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.shortcuts import reverse
from django.template import loader

from main.models import Project
from main.models import User

SITE = Site.objects.get(pk=settings.SITE_ID)


def render_plaintext_message(projects):
    project_texts = []
    for project in projects:
        project_texts.append(
            f"""-------

{project.name} by {project.owner_username} has {project.stars} stars, is mostly written in
{project.language}, and its description reads:

{project.description}

{project.repo_url}

Will you help? Visit the project page to become its maintainer:

https://{SITE.domain}{project.get_absolute_url()}
"""
        )

    project_text = "\n\n".join(project_texts)

    message = f"""Hello, hero.
Projects are in need of aid, and you're the only one who can help.
Here's what was added to {SITE.name} in the past day:

{project_text}

We're counting on you!
The {SITE.name}


P.S. To stop receiving these emails, you can update your settings on
your account page:

https://{SITE.domain}{reverse('account')}
"""

    return message


class Command(BaseCommand):
    help = "Send a digest email for all projects added today."

    def handle(project, *args, **options):
        now = datetime.datetime.utcnow()
        today = datetime.datetime(now.year, now.month, now.day, tzinfo=datetime.timezone.utc)

        projects = Project.objects.filter(created__gte=today - datetime.timedelta(days=1)).order_by("-stars", "name")
        if not projects:
            print("There are no projects to notify about.")
            return

        message = render_plaintext_message(projects)
        html_message = loader.render_to_string("email/new_projects.html", {"projects": projects, "current_site": SITE})

        recipients = User.objects.exclude(receive_notifications=False)
        for recipient in recipients:
            print(f"Emailing {recipient.email}...")
            send_mail(
                f"New projects have joined the {SITE.name}!",
                message,
                settings.DEFAULT_FROM_EMAIL,
                [recipient.email],
                html_message=html_message,
                fail_silently=True,
            )
