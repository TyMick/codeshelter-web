import datetime
from typing import List
from typing import Optional

import jwt
import requests
import shortuuid
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as UM
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db import models
from django.db.models import Count
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from github import Github
from github import GithubException
from gitlab import Gitlab
from gitlab.v4.objects import Project as GLProject
from raven.contrib.django.raven_compat.models import client
from social_django.models import UserSocialAuth

from .exceptions import GithubUnauthorizedException
from djqmethod import Manager
from djqmethod import querymethod


def generate_uuid() -> str:
    """Create a UUID."""
    return shortuuid.ShortUUID().random(20)


class CharIDModel(models.Model):
    """Base model that gives children string IDs."""

    id = models.CharField(max_length=30, primary_key=True, default=generate_uuid, editable=False)

    class Meta:
        abstract = True


class UserManager(Manager, UM):
    """Implement the UserManager by inheriting from the original manager and djqmethod's manager."""

    pass


class User(AbstractUser):
    ROLE = [("MA", "Maintainer"), ("CO", "Contributor")]
    role = models.CharField(max_length=100, choices=ROLE, default=ROLE[1][0])
    maintainer_date = models.DateTimeField(blank=True, null=True, help_text="The date this user became a maintainer.")
    avatar = models.CharField(max_length=1000, blank=True, null=True)
    receive_notifications = models.BooleanField(default=True)

    github_repositories = models.IntegerField(default=0)

    objects = UserManager()

    class Meta:
        ordering = ["date_joined"]

    def send_welcome_email(self):
        """Send an email welcoming the user on account creation."""
        site = Site.objects.get(pk=settings.SITE_ID)

        html_message = loader.render_to_string("email/welcome.html", {"current_site": site})

        message = f"""Hello, hero.
Thank you for signing up to be a contributor. We appreciate your help,
it's very much needed.

Here's how you can start helping:

1. Join our chat server. This is important, as that is our main method
of communication:

http://codeshelter.zulipchat.com/

2. Visit your projects dashboard, pick one or more projects you like
  and start commenting on issues, fixing bugs, adding features and
  submitting PRs:

https://{site.domain}{reverse("home")}

3. If you have extensive open source experience, please apply to
   become a maintainer. Maintainers have commit access to repositories,
   but are otherwise identical to contributors:

https://codeshelter.zulipchat.com/#narrow/stream/150752-maintainer-applications

   If you don't want to become a maintainer, but need someone to merge
   your PRs or help with an issue, just post in the relevant chat topic
   and a maintainer will help out as soon as possible:

https://codeshelter.zulipchat.com/#narrow/stream/186993-projects/topic/project-help

That's pretty much it! If you have any questions or suggestions, please
don't hesitate to open a chat topic about it.

We're counting on you!
The {site.name} community"""

        send_mail(
            f"Welcome to Code Shelter!", message, settings.DEFAULT_FROM_EMAIL, [self.email], html_message=html_message
        )

    def save(self, *args, **kwargs):
        just_created = not self.id

        if self.role == "MA" and not self.maintainer_date:
            # If the user is a maintainer but has no date, it means they just
            # became one, so set it to today.
            self.maintainer_date = timezone.now()
        elif self.role != "MA" and self.maintainer_date:
            # If the user is not a maintainer but has a date, it means they
            # just stopped being one, so set it to None.
            self.maintainer_date = None
        super().save()

        if just_created:
            self.send_welcome_email()

    @querymethod
    def maintainers(query):
        return query.filter(role="MA")

    @querymethod
    def contributors(query):
        return query.filter(role="CO")

    @property
    def github_url(self) -> Optional[str]:
        username = self.github_username
        return f"https://github.com/{username}" if username else None

    @property
    def github_username(self) -> Optional[str]:
        github_auth = self.social_auth.filter(provider="github").first()
        return github_auth.extra_data["login"] if github_auth else None

    def get_full_name(self):
        name = f"{self.first_name} {self.last_name}"
        return name if name.strip() else self.github_username

    def update(self):
        """Update the user from Github."""
        social_auth = self.social_auth.filter(provider="github").first()
        if not social_auth:
            return

        token = social_auth.extra_data["access_token"]
        gu = Github(token).get_user()
        self.avatar = gu.avatar_url
        self.github_repositories = len(list(gu.get_repos()))
        self.save()

    @property
    def is_maintainer(self) -> bool:
        """Return whether the user is an active maintainer or not."""
        return self.role == "MA"

    @property
    def is_contributor(self) -> bool:
        """Return whether the user is an active contributor or not."""
        return self.role == "CO"


class GithubInstallation(CharIDModel):
    """
    A model that contains information about the installation of the GitHub app.
    """

    installation_id = models.CharField(max_length=256, db_index=True)
    owner_username = models.CharField(max_length=1024)

    def __str__(self) -> str:
        return str(f"{self.owner_username}: {self.installation_id}")

    def get_installation_token(self) -> dict:
        headers = {
            "Authorization": b"Bearer " + self._get_installation_jwt(),
            "Accept": "application/vnd.github.machine-man-preview+json",
        }
        response = requests.post(
            f"https://api.github.com/app/installations/{self.installation_id}/access_tokens", headers=headers
        )
        if response.status_code != 201:
            raise GithubUnauthorizedException("Could not access project")
        return response.json()["token"]

    def _get_installation_jwt(self) -> bytes:
        token = jwt.encode(
            {
                "iss": settings.GITHUB_APP_ID,
                "iat": datetime.datetime.utcnow(),
                "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=10),
            },
            settings.GITHUB_PRIVATE_KEY,
            algorithm="RS256",
        )
        return token


class Project(CharIDModel):
    """
    A Project is basically a repository, whether hosted on GitHub, GitLab, etc.
    """

    HOSTS = [("GH", "GitHub"), ("GL", "GitLab")]

    # Repo details.
    host = models.CharField(max_length=1024, choices=HOSTS, default="GH")
    github_installation = models.ForeignKey(GithubInstallation, on_delete=models.CASCADE, blank=True, null=True)
    gitlab_id = models.CharField(max_length=1024, blank=True)

    # The project's name, e.g. "deathstar".
    name = models.CharField(max_length=1024)
    # The owner's username, e.g. "lordvader".
    owner_username = models.CharField(max_length=1024)
    # When the project was added to Code Shelter.
    created = models.DateTimeField(auto_now_add=True)

    # Information about the repo.
    description = models.TextField(blank=True)
    stars = models.IntegerField(blank=True, null=True)
    language = models.CharField(max_length=1000, blank=True)

    # When the above information was last fetched.
    last_updated = models.DateTimeField(default=timezone.now)

    # The Code Shelter members that help maintain this project.
    # These can be either maintainers or contributors.
    maintainers = models.ManyToManyField(User)

    objects = Manager()

    class Meta:
        ordering = ["-stars"]

    @classmethod
    def add_gitlab(cls, gl_project: GLProject):
        """Add a new GitLab project to the database."""
        p, created = cls.objects.update_or_create(
            gitlab_id=str(gl_project.id),
            defaults={
                "host": "GL",
                "name": gl_project.path,
                "owner_username": gl_project.namespace["path"],
                "description": gl_project.description,
                "stars": gl_project.star_count,
            },
        )

        # Don't add the shelter mark here, since it will be added right after, when
        # the management command updates the project.

    @classmethod
    def add_github(cls, repository: dict, payload: dict):
        """Add a new Github project to the database."""
        gi, _ = GithubInstallation.objects.get_or_create(
            installation_id=payload["installation"]["id"],
            defaults={"owner_username": payload["installation"]["account"]["login"]},
        )

        p, created = cls.objects.get_or_create(
            name=repository["name"],
            owner_username=repository["full_name"].split("/")[0],
            defaults={"github_installation": gi},
        )

        p.update()
        p.add_shelter_mark()

    @property
    def is_github(self) -> bool:
        return self.host == "GH"

    @property
    def is_gitlab(self) -> bool:
        return self.host == "GL"

    @querymethod
    def order_by_need(query):
        """
        Order projects by some fitting metric.
        """
        return query.annotate(num_maintainers=Count("maintainers")).order_by("num_maintainers", "-stars", "name")

    @property
    def short_name(self) -> str:
        """Return the project's short name, i.e. `username/reponame`."""
        return f"{self.owner_username}/{self.name}"

    @property
    def repo_url(self) -> str:
        """Return the project's URL."""
        domain = {"GH": "github.com", "GL": "gitlab.com"}[self.host]
        return f"https://{domain}/{self.short_name}"

    def _get_gitlab_api(self):
        assert self.is_gitlab
        return Gitlab("https://www.gitlab.com/", private_token=settings.GITLAB_API_TOKEN)

    def _get_github_api(self):
        assert self.is_github
        token = self.github_installation.get_installation_token()
        return Github(token)

    def get_collaborator_usernames(self) -> List[str]:
        """
        Get the usernames of all the collaborators in the repository.
        """
        try:
            return [u.login for u in self._get_repo().get_collaborators()]
        except GithubException as ge:
            if ge.args[0] == 403:
                raise GithubUnauthorizedException
            else:
                raise

    def get_absolute_url(self) -> str:
        return reverse("project", args=[self.id])

    def create_introductory_issue(self, collaborator: User) -> None:
        """
        Create an issue on the project's issue board introducing the new maintainer to the others.

        This serves as a bit of a social prod to get maintainers talking to each other.
        """
        new_collaborator_username = collaborator.github_username
        existing_collaborator_usernames = self.get_collaborator_usernames()
        plural = len(existing_collaborator_usernames) > 1
        body = (
            "Hello, everyone.\n\n"
            f"@{new_collaborator_username} has just requested to join the project. "
            f"""@{new_collaborator_username}, the existing collaborator{"s" if plural else ""} on this repository """
            f"""{"are" if plural else "is"} @{", @".join(existing_collaborator_usernames)}.\n\n"""
            "It would be great if you could introduce yourselves to each other and talk about the goals of the "
            "project, how maintainance currently works, what the current needs of the project are, etc.\n\n"
            f"Also, @{new_collaborator_username}, please read the [note to maintainers](../blob/master/CODESHELTER.md) "
            "for this project, or [add one](https://gitlab.com/codeshelter/note-to-maintainers) if one doesn't "
            "exist.\n\n"
            "Finally, please join the [Code Shelter chat server](https://codeshelter.zulipchat.com/) if you haven't "
            "already, so we can coordinate better.\n\n"
            f"Thank you for joining, @{new_collaborator_username}!"
        )

        self._get_repo().create_issue(title=f"Introduce the latest maintainer, {new_collaborator_username}", body=body)

    def gh_invite_collaborator(self, collaborator: User) -> None:
        """
        Invite a collaborator to the project on Github.
        """
        u = self._get_github_api().get_user()
        try:
            u._requester.requestJsonAndCheck(
                "PUT", f"/repos/{self.owner_username}/{self.name}/collaborators/{collaborator.github_username}"
            )
        except GithubException as e:
            if "errors" in e.data:
                message = e.data["errors"][0]["message"]
            else:
                message = e.data["message"]
            raise AssertionError(message)
        else:
            self.maintainers.add(collaborator)

        # Create an issue welcoming the new collaborator.
        try:
            self.create_introductory_issue(collaborator)
        except GithubException:
            pass

    def update_collaborators(self):
        """
        Retrieve the project's current collaborators from the repo and update the local database with them.
        """
        if self.is_gitlab:
            # We are all collaborators here.
            return

        # Look up existing maintainers in the project and add them to the project.
        github_collaborators = {
            sa.extra_data["login"]: sa.user
            for sa in UserSocialAuth.objects.filter(provider="github")
            if "login" in sa.extra_data
        }

        for collaborator_username in self.get_collaborator_usernames():
            if collaborator_username in github_collaborators:
                self.maintainers.add(github_collaborators[collaborator_username])

    def update(self):
        """
        Update the project's information from Github.
        """
        repo = self._get_repo()

        if self.is_github:
            self.stars = repo.stargazers_count
            if repo.language:
                self.language = repo.language
            self.description = repo.description.strip(" ⛺") if repo.description else ""
            self.last_updated = timezone.now()
            self.save()
            self.update_collaborators()
        elif self.is_gitlab:
            self.stars = repo.star_count
            languages = repo.languages().items()
            if languages:
                self.language = sorted(languages, key=lambda x: x[1], reverse=True)[0][0]
            self.description = repo.description.strip(" ⛺") if repo.description else ""
            self.last_updated = timezone.now()
            self.save()

    def add_shelter_mark(self):
        """Add a Code Shelter mark to the project."""
        r = self._get_repo()

        if not r.description:
            return

        new_description = r.description.strip(" ⛺") + " ⛺"

        if new_description == r.description:
            return

        try:
            if self.is_github:
                r.edit(description=new_description)
            else:
                r.description = new_description
                r.save()
        except:  # noqa
            # Send all exceptions adding the mark to Sentry, as it's not critical.
            client.captureException()

    def _get_repo(self):
        """Get the project's repository object."""
        cached = getattr(self, "_cached_repo", None)
        if cached:
            return cached

        if self.is_github:
            repo = self._get_github_api().get_repo(self.short_name)
        else:
            repo = self._get_gitlab_api().projects.get(self.gitlab_id)

            # Cache the repo to avoid a second hit.
        self._cached_repo = repo

        return repo

    def __str__(self) -> str:
        return f"{self.owner_username}/{self.name}"
